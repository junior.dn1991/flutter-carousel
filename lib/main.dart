import 'package:flutter/material.dart';
import 'package:carousel/carousel.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  Widget testBGCarousel = new Container(
    child: new Carousel(
      children: [
        new AssetImage('images/cat1.png'),
        new AssetImage('images/cat2.jpg'),
        new AssetImage('images/cat3.gif'),
      ].map((bgImg) => new Image(image: bgImg, width: 1500.0, height: 1500.0, fit: BoxFit.cover)).toList(),
      displayDuration: const Duration(seconds: 5),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Carousel',
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Carousel Demo'),
        ),
        body: new Stack (
            children: <Widget>[
              new PageView(
                children: [testBGCarousel],
              ),
            ]
        ),
      ),
    );
  }
}
